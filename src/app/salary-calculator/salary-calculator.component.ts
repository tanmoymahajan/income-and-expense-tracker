import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-salary-calculator',
  templateUrl: './salary-calculator.component.html',
  styleUrls: ['./salary-calculator.component.scss']
})
export class SalaryCalculatorComponent implements OnInit {

  ctc: number;
  hra: number;
  basic: number;
  ppf: number;
  travel_allowance: number = 19200;
  other_allowance: number;
  total_income: number;
  mandatory_deduction: number = 50000;
  professional_tax: number = 2400;
  corporate_nps: number;
  monthly_rent: number;
  deductible_hra: number;
  mutual_funds: number;
  taxable_income: number;
  total_deductions: number;
  total_deductions_after_tax: number;
  telephone: number;
  sukanya_samriddhi_account: number;
  income_tax: number;
  additional_cess: number;
  monthly_salary: number;
  total_tax: number;
  net_earnings: number;
  showComponents: boolean = false;
  showTaxableSalary: boolean = false;
  isMetro: boolean = false;
  showMonthlySalary = false;

  constructor() { }

  ngOnInit(): void {
  }

  computeComponents() {
    this.showComponents = true;
    this.basic = this.ctc/2;
    this.hra = 0.4 * this.basic;
    this.ppf = 0.12 * this.basic;
    this.other_allowance = this.ctc - (this.basic + this.hra + this.ppf + this.travel_allowance);
  }

  updateComponents() {
    this.ppf = 0.12 * this.basic;
    this.other_allowance = this.ctc - (this.basic + this.hra + this.ppf + this.travel_allowance);
  }

  computeTaxableIncome() {
    this.showTaxableSalary = true;
    this.total_income = this.basic + this.hra + this.travel_allowance + this.other_allowance;
    this.total_deductions = this.computeDeductions();
    this.taxable_income = this.total_income - this.total_deductions;
  }

  computeDeductions(): number {
    this.total_deductions = this.mandatory_deduction + this.professional_tax;
    if (this.deductible_hra) {
      this.total_deductions += this.deductible_hra;
    }
    if (this.corporate_nps) {
      this.total_deductions += this.corporate_nps;
    }
    if (this.telephone) {
      this.total_deductions += this.telephone;
    }

    let sec80c = 0;
    if (this.ppf) {
      sec80c += this.ppf;
    }
    if (this.sukanya_samriddhi_account) {
      sec80c += this.sukanya_samriddhi_account;
    }
    if (this.mutual_funds) {
      sec80c += this.mutual_funds;
    }

    this.total_deductions += Math.min(150000, sec80c);
    return this.total_deductions;
  }

  computeHRA() {
    let rate = 0.4;
    if (this.isMetro) {
      rate = 0.5;
    }

    const option1 = this.hra;
    const option2 = rate * this.basic;
    const option3 = (this.monthly_rent * 12) - (0.1 * this.basic);
    this.deductible_hra = Math.min(option1, option2, option3);

    this.computeTaxableIncome();
  }

  computeTax() {
    this.showMonthlySalary = true;
    if (this.taxable_income <= 250000) {
      this.income_tax = 0;
    } else if (this.taxable_income <= 500000) {
      this.income_tax = (this.taxable_income - 250000) * 0.5;
    } else if (this.taxable_income <= 1000000) {
      this.income_tax = 12500 + ((this.taxable_income - 500000) * 0.20);
    } else {
      this.income_tax = 112500 + ((this.taxable_income - 1000000) * 0.30);
    }

    this.income_tax = Math.ceil(this.income_tax);

    this.additional_cess = Math.ceil(0.04 * this.income_tax);
    this.total_tax = this.income_tax + this.additional_cess;

    this.computeMonthlySalary();
  }

  computeMonthlySalary() {
    this.total_deductions_after_tax = this.income_tax + this.additional_cess + this.corporate_nps + this.ppf + this.professional_tax;
    this.net_earnings = this.total_income - this.total_deductions_after_tax;
    this.monthly_salary = this.net_earnings/12;
  }
}
